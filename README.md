# Reproduction and Replication in Adversarial Stylometry

This repository contains materials for reproducing the results presented in *Reproduction and Replication of an 
Adversarial Stylometry Experiment*.

## Environment Setup

To reproduce the SVM and Logistic Regression results, execute the following commands:

```bash
python3.10 -m venv venv
source venv/bin/activate
python -m pip install -r requirements.txt
```

To reproduce results for all three baselines, use:

```bash
python3.10 -m venv venv
source venv/bin/activate
python -m pip install -r requirements_all.txt
```

Please note: Reproducing deep neural network-based models can be particularly challenging.

After setting up environments, you may also fetch the Riddell-Juola corpus as a submodule, run:

```bash
git submodule update --init --recursive
```

## Command Line Reproduction

Results can be reproduced using the `train.py` module. This module accepts three optional arguments: `-c` (`--corpus`),
`-t` (`--task`), and `-m` (`--model`). For instance, to produce results for the RJ corpus under the control group using
logistic regression (with the Koppel512 feature set), run:

```bash
python -m train -c rj -t control -m logistic_regression
```

The results will display in the terminal and a .json file will be saved in './results' with a descriptive filename 
(e.g., './results/rj_control_logistic_regression.json'). Each experiment may take several minutes to run.

The `-c` argument accepts 'rj' (for the Riddell-Juola corpus) or 'ebg' (for the Extended-Brennan-Greenstadt corpus). 
For both RJ and EBG corpora, 'imitation', 'obfuscation', and 'cross_validation' can be specified for `-t`. 
Additionally, RJ accepts 'control', 'translation_ja', 'translation_de', and 'translation_de_ja'. 
The `-m` argument accepts 'svm' or 'logistic_regression'. When set to 'svm', the "writeprints-static" feature set is 
used; otherwise, the Koppel512 feature set is utilized.

Similar arguments apply when reproducing RoBERTa results. For example, to run a RoBERTa model on the 'rj' corpus for 
the 'control' task ten times, use:

```bash
python -m train_roberta -c rj -t control -r 10
```

## License

Scripts for reproduction are licensed under the 0BSD license. All other materials are subject to the licenses of their
respective owners.

## Contact

hw56@indiana.edu

## Citation

```tex
@article{wang2022reproduction,
  title={Reproduction and Replication of an Adversarial Stylometry Experiment},
  author={Wang, Haining and Juola, Patrick and Riddell, Allen},
  journal={arXiv preprint arXiv:2208.07395},
  year={2022}
}
```